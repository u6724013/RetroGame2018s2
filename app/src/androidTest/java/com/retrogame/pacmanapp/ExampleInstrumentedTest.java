package com.retrogame.pacmanapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.view.MotionEvent;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.retrogame.pacmanapp", appContext.getPackageName());
    }

    /*@Test
    public void test() {
        Ghost ghost0 = new Ghost(20);
        ghost0.setX_Pos(4);
        ghost0.setY_Pos(4);
        Movement m = new Movement(UserInterface.getMap(),20);
        m.moveGhost0();
        System.out.println("After test: "+ghost0.getX_Pos()+" "+ghost0.getY_Pos());
        Assert.assertEquals(7,ghost0.getX_Pos());
    }

    @Test
    public void test1() {
        Movement m = new Movement(UserInterface.getMap(),20);
        m.pelletWasEaten(4,4,(short)5);
        Assert.assertEquals(m.getPelletEaten(),"true");

    }*/
}
