package com.retrogame.pacmanapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by Surekha Gaikwad on 05/10/18
 */
public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_layout);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}