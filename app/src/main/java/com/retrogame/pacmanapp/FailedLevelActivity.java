package com.retrogame.pacmanapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by James Carney on 18/10/18.
 */

public class FailedLevelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failed_level);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Game Over!!!").setNeutralButton("OK", new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dialog, int x) {
                Intent failedIntent = new Intent(PlayActivity.getInstance(), MainActivity.class);
                PlayActivity.getInstance().startActivity(failedIntent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showHelpScreen(View view){
        Intent helpIntent = new Intent(this,HelpActivity.class);
        startActivity(helpIntent);
    }

    public void showPlayScreen(View view) {
        Intent playIntent = new Intent(this,PlayActivity.class);
        startActivity(playIntent);
        PlayActivity.getInstance().finish();
        this.finish();
    }
}
