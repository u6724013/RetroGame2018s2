package com.retrogame.pacmanapp;

/**
 * Created by James Carney on 18/10/18.
 */

public class FailedException extends Exception{

    public FailedException(String message) {
        super(message);
    }
}
