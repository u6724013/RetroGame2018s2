package com.retrogame.pacmanapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class DrawingView extends SurfaceView implements Runnable, SurfaceHolder.Callback {
    private Thread thread;
    private SurfaceHolder holder;
    private boolean draw = true;

    private Paint paint;

    private int totalFrame = 4;
    private int currentPacmanFrame = 0;
    private int currentArrowFrame = 0;
    private long frameTicker;

    private float x1, x2, y1, y2;

    private int screenWidth;
    private int sizeOfBlock;
    final Handler handler = new Handler();
    private short currentMap[][];

    //DrawingView containing separate objects
    private Movement movement;
    private Pacman pacman;
    private Ghost ghost0, ghost1, ghost2, ghost3;
    private BitmapImages bitmap;


    public DrawingView(Context context) {
        super(context);
        paint = new Paint();
        paint.setColor(Color.WHITE);
        holder = getHolder();
        holder.addCallback(this);
        frameTicker = 1000/totalFrame;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        sizeOfBlock = screenWidth/17;
        sizeOfBlock = (sizeOfBlock / 5) * 5;

        currentMap = UserInterface.getMap();

        movement = new Movement(currentMap, sizeOfBlock);

        pacman = movement.getPacman();
        ghost0 = movement.getGhost0();

        ghost1 = movement.getGhost1();
        ghost1.setX_Pos(4 * sizeOfBlock);
        ghost1.setY_Pos(8 * sizeOfBlock);
        ghost1.setDirection(1);

        ghost2 = movement.getGhost2();
        ghost2.setX_Pos(12 * sizeOfBlock);
        ghost2.setY_Pos(8 * sizeOfBlock);
        ghost2.setDirection(2);

        ghost3 = movement.getGhost3();
        ghost3.setX_Pos(4 * sizeOfBlock);
        ghost3.setY_Pos(0 * sizeOfBlock);
        ghost3.setDirection(3);


        bitmap = new BitmapImages(sizeOfBlock, context);
    }

    // code added by Haosen Yin on 13/10/18
    @Override
    public void run() {
        Log.i("info", "Run");
        GameConditions.countNoOfPellets(currentMap);
        while (draw) {
            if (!holder.getSurface().isValid()) {
                continue;
            }
            Canvas canvas = holder.lockCanvas();
            if (canvas != null) {
                displayGame(canvas);
            }
        }
    }

    // refactored code by Surekha Gaikwad on 19/10/18
    public void displayGame(Canvas canvas) {
        int type = 0;
        canvas.drawColor(Color.BLACK);
        UserInterface.drawMap(canvas, currentMap, paint, sizeOfBlock);
        changeFrame(System.currentTimeMillis());
        Ghost.displayGhost(ghost0, bitmap, canvas, movement, paint, getContext(), type);
        Ghost.displayGhost(ghost1, bitmap, canvas, movement, paint, getContext(), type + 1);
        Ghost.displayGhost(ghost2, bitmap, canvas, movement, paint, getContext(), type + 2);
        Ghost.displayGhost(ghost3, bitmap, canvas, movement, paint, getContext(), type + 3);
        pacman.drawPacman(bitmap, canvas, movement, paint, getContext(), currentPacmanFrame);
        updateConditions(movement);
        UserInterface.displayPauseButton(bitmap, canvas, paint, sizeOfBlock);
        UserInterface.displayArrow(bitmap, movement, canvas, paint, sizeOfBlock, currentArrowFrame);
        UserInterface.displayPellets(canvas, currentMap, paint, sizeOfBlock);
        UserInterface.displayScores(canvas, paint, sizeOfBlock);
        holder.unlockCanvasAndPost(canvas);
    }

    private void changeFrame(long timeOfGame) {
        if(timeOfGame > frameTicker + (totalFrame * 30)) {
            frameTicker = timeOfGame;
            currentPacmanFrame++;
            if(currentPacmanFrame >= totalFrame) {
                currentPacmanFrame = 0;
            }
        }
        if(timeOfGame > frameTicker + (50)) {
            currentArrowFrame++;
            if(currentArrowFrame >= 7) {
                currentArrowFrame = 0;
                currentArrowFrame = 0;
            }
        }
    }

    private void updateConditions(Movement movement) {
        if(movement.needMapRefresh()) {
            GameConditions gameConditions = GameConditions.getInstance();
            currentMap = movement.updateMap();
            gameConditions.updateCurrentScore();
            gameConditions.updateNoOfPellets();
        }
    }

    Runnable pauseThread = new Runnable() {
        public void run() {
            Log.i("info","pauseThread");
            Intent pauseIntent = new Intent(getContext(),PauseActivity.class);
            getContext().startActivity(pauseIntent);
        }
    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case (MotionEvent.ACTION_DOWN): {
                x1 = event.getX();
                y1 = event.getY();
                if (x1 >= sizeOfBlock*0 && x1 <= sizeOfBlock*4 && y1 >= sizeOfBlock*20 && y1 <= sizeOfBlock*22) {
                    handler.postAtFrontOfQueue(pauseThread);
                }
                break;
            }
            case (MotionEvent.ACTION_UP): {
                x2 = event.getX();
                y2 = event.getY();
                calculateSwipeDirection();
                if (x1 >= sizeOfBlock*0 && x1 <= sizeOfBlock*4 && y1 >= sizeOfBlock*20 && y1 <= sizeOfBlock*22) {
                    handler.removeCallbacks(pauseThread);
                }
                break;
            }
        }
        return true;
    }



    // code added by Haosen Yin on 13/10/18
    // calculating swipeDirection for moving pacman object
    private void calculateSwipeDirection() {
        float xDiff = (x2 - x1);
        float yDiff = (y2 - y1);
        if (Math.abs(yDiff) > Math.abs(xDiff)) {
            if (yDiff < 0) {
                pacman.setNextDir(0);
            } else if (yDiff > 0) {
                pacman.setNextDir(2);
            }
        } else {
            if (xDiff < 0) {
                pacman.setNextDir(3);
            } else if (xDiff > 0) {
                pacman.setNextDir(1);
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i("info", "Surface Created");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i("info", "Surface Changed");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i("info", "Surface Destroyed");
    }

    public void pause() {
        Log.i("info", "pause");
        draw = false;
        thread = null;
    }

    public void resume() {
        Log.i("info", "resume");
        if (thread != null) {
            thread.start();
        }
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
            Log.i("info", "resume thread");
        }
        draw = true;
    }


}
