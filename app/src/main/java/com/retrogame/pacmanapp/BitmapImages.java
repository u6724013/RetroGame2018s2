package com.retrogame.pacmanapp;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Surekha Gaikwad on 05/10/18.
 */

public class BitmapImages {

    private int spriteSize, arrowSize, buttonSizeHeight, buttonSizeWidth;
    private Bitmap[] pacmanRight, pacmanDown, pacmanLeft, pacmanUp;
    private Bitmap[] rightArrow, downArrow, leftArrow, upArrow;
    private Bitmap ghostBitmap0, ghostBitmap1, ghostBitmap2, ghostBitmap3;
    private Bitmap pauseBitmap;

    public BitmapImages(int blockSize, Context context){
        spriteSize = blockSize;
        arrowSize = blockSize*7;
        buttonSizeHeight = blockSize*2;
        buttonSizeWidth = blockSize*4;

        // bitmap images of right arrow indicators
        rightArrow = new Bitmap[7];
        rightArrow[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame1), arrowSize, arrowSize, false);
        rightArrow[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame2), arrowSize, arrowSize, false);
        rightArrow[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame3), arrowSize, arrowSize, false);
        rightArrow[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame4), arrowSize, arrowSize, false);
        rightArrow[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame5), arrowSize, arrowSize, false);
        rightArrow[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame6), arrowSize, arrowSize, false);
        rightArrow[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.right_arrow_frame7), arrowSize, arrowSize, false);


        downArrow = new Bitmap[7];
        downArrow[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame1), arrowSize, arrowSize, false);
        downArrow[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame2), arrowSize, arrowSize, false);
        downArrow[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame3), arrowSize, arrowSize, false);
        downArrow[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame4), arrowSize, arrowSize, false);
        downArrow[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame5), arrowSize, arrowSize, false);
        downArrow[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame6), arrowSize, arrowSize, false);
        downArrow[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.down_arrow_frame7), arrowSize, arrowSize, false);


        upArrow = new Bitmap[7];
        upArrow[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame1), arrowSize, arrowSize, false);
        upArrow[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame2), arrowSize, arrowSize, false);
        upArrow[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame3), arrowSize, arrowSize, false);
        upArrow[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame4), arrowSize, arrowSize, false);
        upArrow[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame5), arrowSize, arrowSize, false);
        upArrow[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame6), arrowSize, arrowSize, false);
        upArrow[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.up_arrow_frame7), arrowSize, arrowSize, false);


        leftArrow = new Bitmap[7];
        leftArrow[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame1), arrowSize, arrowSize, false);
        leftArrow[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame2), arrowSize, arrowSize, false);
        leftArrow[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame3), arrowSize, arrowSize, false);
        leftArrow[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame4), arrowSize, arrowSize, false);
        leftArrow[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame5), arrowSize, arrowSize, false);
        leftArrow[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame6), arrowSize, arrowSize, false);
        leftArrow[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.left_arrow_frame7), arrowSize, arrowSize, false);


        // bitmap images of pacman facing right
        pacmanRight = new Bitmap[4];
        pacmanRight[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(),R.drawable.pacman_right1), spriteSize, spriteSize, false);
        pacmanRight[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_right2), spriteSize, spriteSize, false);
        pacmanRight[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_right3), spriteSize, spriteSize, false);
        pacmanRight[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_right), spriteSize, spriteSize, false);


        // bitmap images of pacman facing down
        pacmanDown = new Bitmap[4];
        pacmanDown[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_down1), spriteSize, spriteSize, false);
        pacmanDown[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_down2), spriteSize, spriteSize, false);
        pacmanDown[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_down3), spriteSize, spriteSize, false);
        pacmanDown[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_down), spriteSize, spriteSize, false);


        // bitmap images of pacman facing left
        pacmanLeft = new Bitmap[4];
        pacmanLeft[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_left1), spriteSize, spriteSize, false);
        pacmanLeft[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_left2), spriteSize, spriteSize, false);
        pacmanLeft[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_left3), spriteSize, spriteSize, false);
        pacmanLeft[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_left), spriteSize, spriteSize, false);


        // bitmap images of pacman facing up
        pacmanUp = new Bitmap[4];
        pacmanUp[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_up1), spriteSize, spriteSize, false);
        pacmanUp[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_up2), spriteSize, spriteSize, false);
        pacmanUp[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_up3), spriteSize, spriteSize, false);
        pacmanUp[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pacman_up), spriteSize, spriteSize, false);


        // bitmap image of pause
        pauseBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pause_button), buttonSizeWidth, buttonSizeHeight, false);

        // bitmap image of ghost
        ghostBitmap0 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.ghost0), spriteSize, spriteSize, false);

        // bitmap image of ghost
        ghostBitmap1 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.ghost1), spriteSize, spriteSize, false);

        // bitmap image of ghost
        ghostBitmap2 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.ghost2), spriteSize, spriteSize, false);

        // bitmap image of ghost
        ghostBitmap3 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.ghost3), spriteSize, spriteSize, false);
    }

    public Bitmap getPauseBitmap(){ return pauseBitmap; }

    public Bitmap[] getPacmanRight(){ return pacmanRight; }
    public Bitmap[] getPacmanLeft(){ return pacmanLeft; }
    public Bitmap[] getPacmanUp(){ return pacmanUp; }
    public Bitmap[] getPacmanDown(){ return pacmanDown; }

    public Bitmap getGhostBitmap0(){ return ghostBitmap0; }
    public Bitmap getGhostBitmap1(){ return ghostBitmap1; }
    public Bitmap getGhostBitmap2(){ return ghostBitmap2; }
    public Bitmap getGhostBitmap3(){ return ghostBitmap3; }

    public Bitmap[] getArrowRight(){ return rightArrow; }
    public Bitmap[] getArrowLeft(){ return leftArrow; }
    public Bitmap[] getArrowUp(){ return upArrow; }
    public Bitmap[] getArrowDown(){ return downArrow; }
}
