package com.retrogame.pacmanapp;

import android.content.Context;
import android.util.Log;

/**
 * Created by James Carney on 18/10/18.
 */

public class GameConditions {

    private static GameConditions instance;
    private int currentScore = 0;
    private int noOfPellets;

    private GameConditions() {

    }

    public static synchronized GameConditions getInstance() {
        if(instance==null){
            instance=new GameConditions();
        }
        return instance;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public int getNoOfPellets() {
        return noOfPellets;
    }

    public void setNoOfPellets(int noOfPellets) {
        this.noOfPellets = noOfPellets;
    }

    public void updateCurrentScore() {
        currentScore += 10;
    }

    public void updateNoOfPellets() {
        noOfPellets--;
    }

    public static void resetCurrentScore() {
        GameConditions gameConditions = GameConditions.getInstance();
        gameConditions.setCurrentScore(0);
    }

    public static void countNoOfPellets(short[][] currentMap){
        GameConditions gameConditions = GameConditions.getInstance();
        gameConditions.setNoOfPellets(0);
        for(int i=0;i<18;i++){
            for(int j=0;j<17;j++){
                if((currentMap[i][j] & 16) != 0){
                    gameConditions.setNoOfPellets(gameConditions.getNoOfPellets() + 1 );
                    Log.i("info","Pellets = " + Integer.toString(gameConditions.getNoOfPellets()));
                }
            }
        }
    }
}
