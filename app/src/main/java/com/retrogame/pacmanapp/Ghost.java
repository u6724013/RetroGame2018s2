package com.retrogame.pacmanapp;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Surekha Gaikwad on 05/10/18.
 */
public class Ghost {
    private int x_Pos, y_Pos, direction;

    public Ghost(int blockSize){

        x_Pos = 8 * blockSize;
        y_Pos = 4 * blockSize;

        direction = 4;
    }

    public int getX_Pos(){ return x_Pos; }
    public int getY_Pos(){ return y_Pos; }
    public int getDirection(){ return direction; }

    public void setX_Pos(int x_Pos){ this.x_Pos = x_Pos; }
    public void setY_Pos(int y_Pos){ this.y_Pos = y_Pos; }
    public void setDirection(int dir){ this.direction = dir; }

    // refactored code by Surekha Gaikwad on 19/10/18
    public static void displayGhost(Ghost ghost, BitmapImages bitmap, Canvas canvas, Movement movement, Paint paint, Context context, int type) {
        movement.moveGhost(ghost);
        switch(type) {
            case 0 : canvas.drawBitmap(bitmap.getGhostBitmap0(), ghost.getX_Pos(), ghost.getY_Pos(), paint); break;
            case 1 : canvas.drawBitmap(bitmap.getGhostBitmap1(), ghost.getX_Pos(), ghost.getY_Pos(), paint); break;
            case 2 : canvas.drawBitmap(bitmap.getGhostBitmap2(), ghost.getX_Pos(), ghost.getY_Pos(), paint); break;
            case 3 : canvas.drawBitmap(bitmap.getGhostBitmap3(), ghost.getX_Pos(), ghost.getY_Pos(), paint); break;

        }
        movement.failedGame(context);
    }
}
