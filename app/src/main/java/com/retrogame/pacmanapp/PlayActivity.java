package com.retrogame.pacmanapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Surekha Gaikwad on 05/10/2018
 */
public class PlayActivity extends AppCompatActivity {

    static PlayActivity activity;
    private SharedPreferences sharedPreferences;
    private DrawingView drawingView;
  //  private Globals globals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawingView = new DrawingView(this);
        setContentView(drawingView);
        activity = this;
    }

    // code added by Haosen Yin on 13/10/18
    @Override
    protected void onPause() {
        Log.i("info", "onPause");
        super.onPause();
        drawingView.pause();
    }

    // code added by Haosen Yin on 13/10/18
    @Override
    protected void onResume() {
        Log.i("info", "onResume");
        super.onResume();
        drawingView.resume();
    }

    // code added by Haosen Yin on 18/10/18
    public static PlayActivity getInstance() {
        return activity;
    }

}
