package com.retrogame.pacmanapp;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Surekha Gaikwad on 05/10/18.
 */

public class Movement {
    private Pacman pacman;

    private Ghost ghost0;
    private Ghost ghost1;
    private Ghost ghost2;
    private Ghost ghost3;

    private int sizeOfBlock;
    private short [][] currentMap;
    private int swipeDirection;
    private boolean pelletRemoved;

    public boolean getPelletEaten() {
        return pelletRemoved;
    }

    public Movement(final short [][] curMap, final int sizeOfBlock){
        currentMap = curMap;
        this.sizeOfBlock = sizeOfBlock;
        pacman = new Pacman(sizeOfBlock);
        ghost0 = new Ghost(sizeOfBlock);
        ghost1 = new Ghost(sizeOfBlock);
        ghost2 = new Ghost(sizeOfBlock);
        ghost3 = new Ghost(sizeOfBlock);

        swipeDirection = 4;
        pelletRemoved = false;
    }


    //updates map when pellet is removed
    public boolean needMapRefresh(){ return pelletRemoved; }

    //change map when pellet is removed
    public void pelletWasEaten(int x, int y, short value){
        currentMap[x][y] = value;
        pelletRemoved = true;
    }

    // code added by Haosen Yin on 13/10/18
    public void movePacman(){
        short change;
        int nextDirection = pacman.getNextDir();
        int xPosPacman = pacman.getXPos();
        int yPosPacman = pacman.getYPos();

        if ( (xPosPacman % sizeOfBlock == 0) && (yPosPacman  % sizeOfBlock == 0) ) {

            if (xPosPacman >= sizeOfBlock * 17) {
                xPosPacman = 0;
                pacman.setXPos(0);
            }

            change = currentMap[yPosPacman / sizeOfBlock][xPosPacman / sizeOfBlock];

            if ((change & 16) != 0) {

                pelletWasEaten(yPosPacman / sizeOfBlock, xPosPacman / sizeOfBlock, (short) (change ^ 16));
            }
            if (!((nextDirection == 3 && (change & 1) != 0) || (nextDirection == 1 && (change & 4) != 0) || (nextDirection == 0 && (change & 2) != 0) ||
                    (nextDirection == 2 && (change & 8) != 0))) {
                pacman.setCurDir(nextDirection);
                swipeDirection = nextDirection;
            }

            if ((swipeDirection == 3 && (change & 1) != 0) || (swipeDirection == 1 && (change & 4) != 0) || (swipeDirection == 0 && (change & 2) != 0) ||
                    (swipeDirection == 2 && (change & 8) != 0)) {
                swipeDirection = 4;
            }
        }

        if (xPosPacman < 0) {
            xPosPacman = sizeOfBlock * 17;
            pacman.setXPos(sizeOfBlock * 17);
        }
    }

    //call method after we have moved and drawn pacman
    public void updatePacman(){
        if (swipeDirection == 0) {
            pacman.setYPos(pacman.getYPos() + -sizeOfBlock/15);
        } else if (swipeDirection == 1) {
            pacman.setXPos(pacman.getXPos() + sizeOfBlock/15);
        } else if (swipeDirection == 2) {
            pacman.setYPos(pacman.getYPos() + sizeOfBlock/15);
        } else if (swipeDirection == 3) {
            pacman.setXPos(pacman.getXPos() + -sizeOfBlock/15);
        }
    }

    // refactored code by Surekha Gaikwad on 19/10/18
    // code added by Haosen Yin on 13/10/18
    //move ghost
    public void moveGhost(Ghost ghost) {
        short change;
        int ghostDir = ghost.getDirection();
        int xPosGhost = ghost.getX_Pos();
        int yPosGhost = ghost.getY_Pos();
        int xDis = pacman.getXPos() - xPosGhost;
        int yDis = pacman.getYPos() - yPosGhost;


        if ((xPosGhost % sizeOfBlock == 0) && (yPosGhost % sizeOfBlock == 0)) {
            change = currentMap[yPosGhost / sizeOfBlock][xPosGhost / sizeOfBlock];

            if (xPosGhost >= sizeOfBlock * 17) {
                xPosGhost = 0;
                ghost.setX_Pos(0);
            }
            if (xPosGhost < 0) {
                xPosGhost = sizeOfBlock * 17;
                ghost.setX_Pos(sizeOfBlock * 17);
            }

            if (xDis >= 0 && yDis >= 0) {
                if ((change & 4) == 0 && (change & 8) == 0) {
                    if (Math.abs(xDis) > Math.abs(yDis)) {
                        ghostDir = 1;
                        ghost.setDirection(1);
                    } else {
                        ghostDir = 2;
                        ghost.setDirection(2);
                    }
                } else if ((change & 4) == 0) {
                    ghostDir = 1;
                    ghost.setDirection(1);
                } else if ((change & 8) == 0) {
                    ghostDir = 2;
                    ghost.setDirection(2);
                } else {
                    ghostDir = 3;
                    ghost.setDirection(3);
                }
            }

            if (xDis >= 0 && yDis <= 0) {
                if (xDis >= 0 && yDis <= 0) {
                    if ((change & 4) == 0 && (change & 2) == 0) {
                        if (Math.abs(xDis) > Math.abs(yDis)) {
                            ghostDir = 1;
                            ghost.setDirection(1);
                        } else {
                            ghostDir = 0;
                            ghost.setDirection(0);
                        }
                    } else if ((change & 4) == 0) {
                        ghostDir = 1;
                        ghost.setDirection(1);
                    } else if ((change & 2) == 0) {
                        ghostDir = 0;
                        ghost.setDirection(0);
                    } else {
                        ghostDir = 2;
                        ghost.setDirection(2);
                    }
                }
            }
            if (xDis <= 0 && yDis >= 0) {
                if ((change & 1) == 0 && (change & 8) == 0) {
                    if (Math.abs(xDis) > Math.abs(yDis)) {
                        ghostDir = 3;
                        ghost.setDirection(3);
                    } else {
                        ghostDir = 2;
                        ghost.setDirection(2);
                    }
                } else if ((change & 1) == 0) {
                    ghostDir = 3;
                    ghost.setDirection(3);
                } else if ((change & 8) == 0) {
                    ghostDir = 2;
                    ghost.setDirection(2);
                } else {
                    ghostDir = 1;
                    ghost.setDirection(1);
                }
            }
            if (xDis <= 0 && yDis <= 0) {
                if ((change & 1) == 0 && (change & 2) == 0) {
                    if (Math.abs(xDis) > Math.abs(yDis)) {
                        ghostDir = 3;
                        ghost.setDirection(3);
                    } else {
                        ghostDir = 0;
                        ghost.setDirection(0);
                    }
                } else if ((change & 1) == 0) {
                    ghostDir = 3;
                    ghost.setDirection(3);
                } else if ((change & 2) == 0) {
                    ghostDir = 0;
                    ghost.setDirection(0);
                } else {
                    ghostDir = 2;
                    ghost.setDirection(2);
                }
            }

            if ((ghostDir == 3 && (change & 1) != 0) || (ghostDir == 1 && (change & 4) != 0) || (ghostDir == 0 && (change & 2) != 0) ||
                    (ghostDir == 2 && (change & 8) != 0)) {
                ghostDir = 4;
                ghost.setDirection(4);
            }
        }

        if (ghost.getDirection() == 0) {
            ghost.setY_Pos(ghost.getY_Pos() + -sizeOfBlock/20);
        } else if (ghost.getDirection() == 1) {
            ghost.setX_Pos(ghost.getX_Pos() + sizeOfBlock/20);
        } else if (ghost.getDirection() == 2) {
            ghost.setY_Pos(ghost.getY_Pos() + sizeOfBlock/20);
        } else if (ghost.getDirection() == 3) {
            ghost.setX_Pos(ghost.getX_Pos() + -sizeOfBlock/20);
        }
    }


    // code added by James Carney on 18/10/18
    public void checkPlayerFailed() throws FailedException {

        if((ghost0.getX_Pos()/sizeOfBlock == (pacman.getXPos()/sizeOfBlock)) && ((ghost0.getY_Pos()/sizeOfBlock) == (pacman.getYPos()/sizeOfBlock)) ||
            ((ghost1.getX_Pos()/sizeOfBlock) == (pacman.getXPos()/sizeOfBlock)) && ((ghost1.getY_Pos()/sizeOfBlock) == (pacman.getYPos()/sizeOfBlock)) ||
            ((ghost2.getX_Pos()/sizeOfBlock) == (pacman.getXPos()/sizeOfBlock)) && ((ghost2.getY_Pos()/sizeOfBlock) == (pacman.getYPos()/sizeOfBlock)) ||
            ((ghost3.getX_Pos()/sizeOfBlock) == (pacman.getXPos()/sizeOfBlock)) && ((ghost3.getY_Pos()/sizeOfBlock) == (pacman.getYPos()/sizeOfBlock))) {
            throw  new FailedException("Collision");
        }

    }

    // code added by James Carney on 18/10/18
    public void failedGame(Context context) {
        try{
            checkPlayerFailed();
        } catch(FailedException e) {
            Intent failedIntent = new Intent(context, FailedLevelActivity.class);
            context.startActivity(failedIntent);
        }
    }

    public short[][] updateMap() {
        pelletRemoved = false;
        return currentMap;
    }

    public boolean refreshMap() {
        return pelletRemoved ;
    }

    public Pacman getPacman(){ return pacman; }
    public Ghost getGhost0(){ return ghost0; }
    public Ghost getGhost1(){ return ghost1; }
    public Ghost getGhost2(){ return ghost2; }
    public Ghost getGhost3(){ return ghost3; }
}
