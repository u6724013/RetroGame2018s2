package com.retrogame.pacmanapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Surekha Gaikwad on 05/10/18.
 */

public class Pacman {
    
    private int xPos, yPos, curDir, nextDir;
    public Pacman(final int blockSize){

        xPos = 8 * blockSize;
        yPos = 13 * blockSize;

        curDir = 2;
        nextDir = 4;
    }

    
    public int getXPos(){ return xPos; }
    public int getYPos(){ return yPos; }
    public int getCurDir(){ return curDir; }
    public int getNextDir(){ return nextDir; }

    
    public void setXPos(int xPos){ this.xPos = xPos; }
    public void setYPos(int yPos){ this.yPos = yPos; }
    public void setCurDir(int curDir){ this.curDir = curDir; }
    public void setNextDir(int nextDir){ this.nextDir = nextDir; }

    //// code added by Haosen Yin on 13/10/18
    public void drawPacman(BitmapImages bitmap, Canvas canvas, Movement movement, Paint paint, Context context, int currentPacmanFrame) {
        
        movement.movePacman();
        
        switch (this.getCurDir()) {
            case (0):
                canvas.drawBitmap(bitmap.getPacmanUp()[currentPacmanFrame], this.getXPos(), this.getYPos(), paint);
                break;
            case (1):
                canvas.drawBitmap(bitmap.getPacmanRight()[currentPacmanFrame], this.getXPos(), this.getYPos(), paint);
                break;
            case (3):
                canvas.drawBitmap(bitmap.getPacmanLeft()[currentPacmanFrame], this.getXPos(), this.getYPos(), paint);
                break;
            default:
                canvas.drawBitmap(bitmap.getPacmanDown()[currentPacmanFrame], this.getXPos(), this.getYPos(), paint);
                break;
        }
        movement.updatePacman();
    }
}
