package com.retrogame.pacmanapp;

/**
 * Created by James Carney on 18/10/18.
 */

public class GlobalScore {

    private static GlobalScore instance;

    private int highScore;

    private GlobalScore(){

    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    public static synchronized  GlobalScore getInstance(){
        if(instance == null) {
            instance = new GlobalScore();
        }
        return instance;
    }
}
