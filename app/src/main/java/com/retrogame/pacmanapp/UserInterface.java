package com.retrogame.pacmanapp;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Surekha Gaikwad on 18/10/18.
 */

public class UserInterface {
    private UserInterface(){}

    public static void displayArrow(BitmapImages bitmap, Movement movement, Canvas canvas, Paint paint, int blockSize, int currentArrowFrame) {
        switch(movement.getPacman().getNextDir()) {
            case(0):
                canvas.drawBitmap(bitmap.getArrowUp()[currentArrowFrame],5*blockSize , 20*blockSize, paint);
                break;
            case(1):
                canvas.drawBitmap(bitmap.getArrowRight()[currentArrowFrame],5*blockSize , 20*blockSize, paint);
                break;
            case(2):
                canvas.drawBitmap(bitmap.getArrowDown()[currentArrowFrame],5*blockSize , 20*blockSize, paint);
                break;
            case(3):
                canvas.drawBitmap(bitmap.getArrowLeft()[currentArrowFrame],5*blockSize , 20*blockSize, paint);
                break;
            default:
                break;
        }
    }

    public static void displayPauseButton(BitmapImages bitmap,Canvas canvas, Paint paint, int blockSize) {

        canvas.drawBitmap(bitmap.getPauseBitmap(),0, 20*blockSize, paint);
    }

    // code added by Haosen Yin on 18/10/18
    // draws pellets and updates them when removed
    public static void displayPellets(Canvas canvas, short[][] currentMap, Paint paint, int blockSize) {
        float x, y;
        for (int i = 0; i < 18; i++) {
            for (int j = 0; j < 17; j++) {
                x = j * blockSize;
                y = i * blockSize;
                if ((currentMap[i][j] & 16) != 0) {
                    canvas.drawCircle(x + blockSize / 2, y + blockSize / 2, blockSize / 10, paint);
                }
            }
        }
    }

    // code added by Haosen Yin on 18/10/18
    // draws map layout
    public static void drawMap(Canvas canvas, short[][] currentMap, Paint paint, int blockSize) {
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(2.5f);
        int x, y;

        for (int i = 0; i < 18; i++) {
            for (int j = 0; j < 17; j++) {
                x = j * blockSize;
                y = i * blockSize;
                if ((currentMap[i][j] & 1) != 0) // draws left
                    canvas.drawLine(x, y, x, y + blockSize - 1, paint);

                if ((currentMap[i][j] & 2) != 0) // draws top
                    canvas.drawLine(x, y, x + blockSize - 1, y, paint);

                if ((currentMap[i][j] & 4) != 0) // draws right
                    canvas.drawLine(
                            x + blockSize, y, x + blockSize, y + blockSize - 1, paint);
                if ((currentMap[i][j] & 8) != 0) // draws bottom
                    canvas.drawLine(
                            x, y + blockSize, x + blockSize - 1, y + blockSize , paint);
            }
        }
        paint.setColor(Color.WHITE);
    }

    // code added by Haosen Yin on 18/10/18
    public static void displayScores(Canvas canvas, Paint paint, int blockSize) {
        paint.setTextSize(blockSize);

        GlobalScore globalScore = GlobalScore.getInstance();
        GameConditions gameConditions = GameConditions.getInstance();
        int currentScore = gameConditions.getCurrentScore();
        int highScore = globalScore.getHighScore();
        if(currentScore > highScore) {
            globalScore.setHighScore(currentScore);
        }

        String displayHighScore = String.format("%05d", highScore);
        String high_Score = "High score : " + displayHighScore ;
        canvas.drawText(high_Score,0,2*blockSize - 10,paint);

        String displayScore = String.format("%05d", currentScore);
        String score = "Score : " + displayScore;
        canvas.drawText(score, 11*blockSize,2*blockSize - 10,paint);
    }

    // code added by Haosen Yin on 18/10/18
    //Map that has all pellets removed except for one
    public static short[][] getMap(){
        return new short[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {19, 26, 26, 18, 26, 26, 26, 22, 0, 19, 26, 26, 26, 18, 26, 26, 22},
                {21, 0, 0, 21, 0, 0, 0, 21, 0, 21, 0, 0, 0, 21, 0, 0, 21},
                {17, 26, 26, 16, 26, 18, 26, 24, 26, 24, 26, 18, 26, 16, 26, 26, 20},
                {25, 26, 26, 20, 0, 25, 26, 22, 0, 19, 26, 28, 0, 17, 26, 26, 28},
                {0, 0, 0, 21, 0, 0, 0, 21, 0, 21, 0, 0, 0, 21, 0, 0, 0},
                {0, 0, 0, 21, 0, 19, 26, 24, 26, 24, 26, 22, 0, 21, 0, 0, 0},
                {26, 26, 26, 16, 26, 20, 0, 0, 0, 0, 0, 17, 26, 16, 26, 26, 26},
                {0, 0, 0, 21, 0, 17, 26, 26, 26, 26, 26, 20, 0, 21, 0, 0, 0},
                {0, 0, 0, 21, 0, 21, 0, 0, 0, 0, 0, 21, 0, 21, 0, 0, 0},
                {19, 26, 26, 16, 26, 24, 26, 22, 0, 19, 26, 24, 26, 16, 26, 26, 22},
                {21, 0, 0, 21, 0, 0, 0, 21, 0, 21, 0, 0, 0, 21, 0, 0, 21},
                {25, 22, 0, 21, 0, 0, 0, 17, 2, 20, 0, 0, 0, 21, 0, 19, 28},
                {0, 21, 0, 17, 26, 26, 18, 24, 24, 24, 18, 26, 26, 20, 0, 21, 0},
                {19, 24, 26, 28, 0, 0, 25, 18, 26, 18, 28, 0, 0, 25, 26, 24, 22},
                {21, 0, 0, 0, 0, 0, 0, 21, 0, 21, 0, 0, 0, 0, 0, 0, 21},
                {25, 26, 26, 26, 26, 26, 26, 24, 26, 24, 26, 26, 26, 26, 26, 26, 28},
        };
    }
}
